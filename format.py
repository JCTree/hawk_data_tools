import csv
import pandas as pd
from tkinter import Tk
from tkinter.filedialog import askopenfilename, asksaveasfilename

Tk().withdraw()
input = askopenfilename()

data = []
with open(input) as f:
    raw_data = csv.reader(f)
    num_blank_lines = 0
    for line in raw_data:
        if line == []:
            num_blank_lines+= 1
            continue
        if num_blank_lines == 2:
            data.append(line)
                
# Consolidate titles to one line
df = pd.DataFrame(data[2:], columns = 
                  [a.strip() + " " + b.lstrip() for a, b in zip(data[0], data[1])])

# Remove the last line if it is not fully written
if df.iat[-1,-1] is None:
    df.drop(df.tail(1).index, inplace=True)

# Create values for deadtime column
deadtime = 1 / (1 - pd.to_numeric(df['Total Counts (cpm)']) * 1.07E-6)

# Gather required data for dose rate calculation
doses = pd.to_numeric(df['Total Dose Equ.'])
dose_units = df['Units Dose Equ.']
gammas = pd.to_numeric(df['Gamma Dose <10kev/um'])
gamma_units = df['Units Gamma Dose']

# Calculate dose rate in mrem/hr
dose_rate = []
for dose, dose_unit, gamma, gamma_unit, dt in \
    zip(doses, dose_units, gammas, gamma_units, deadtime):
    
    if dose_unit.strip() == "nS" and gamma_unit.strip() == "nG":
        dose_rate.append((dose-gamma)*6*dt/1000)
    elif dose_unit.strip() == "uS" and gamma_unit.strip() == "uG":
        dose_rate.append((dose-gamma)*6*dt)
    elif dose_unit.strip() == "uS" and gamma_unit.strip() == "nG":
        dose_rate.append((dose-gamma/1000)*6*dt)
    else:
        dose_rate.append("#VALUE")

# Add deadtime and dose rate to dataframe
df.insert(len(df.columns), "Deadtime", deadtime)
df.insert(len(df.columns), "Dose Rate (mrem/hr)", dose_rate)

types = [('Excel Spreadsheet', '*.xlsx')]

output = asksaveasfilename(filetypes=types, defaultextension=types)
df.to_excel(output)
